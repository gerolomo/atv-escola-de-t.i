import React, { Component } from 'react';
import { Button, ButtonToolbar, Spinner, Table, Modal } from 'react-bootstrap';


import { listClientes, saveClientes, saveEditClienteService, removeCliente } from '../../services/serviceCliente.js'


export default class Clientes extends Component {

  state = {
    clientes: [],
    clientesNotFiltered: [],
    loading: true,
    loadingSave: false,
    new: false,
    search: '',
    text: '',
    CPF: '',
    email: ''
  }

  async componentDidMount() {
    this.setState({
      clientesNotFiltered: await listClientes(),
      loading: false
    })
    this.filter()
  }


  newCliente = () => {
    this.setState({
      new: true
    })
  }

  editText = (event) => {
    this.setState({
      text: event.target.value
    })
  }

  saveCliente = async () => {
    this.setState({
      loadingSave: true
    })
    await saveClientes(this.state.text, this.state.CPF, this.state.email)
    this.setState({
      clientes: await listClientes(),
      loadingSave: false,
      new: false,
      text: '',
      CPF: '',
      email: ''
    })
  }

  keyPress = (event) => {
    if (event.key === 'Enter') {
      this.saveCliente()
    }
  }


  setFilter = (event) => {
    this.setState({
      search: event.target.value
    })
    this.filter(event.target.value)
  }

  filter = (value) => {
    value = !value ? this.state.search : value
    this.setState({
      clientes: this.state.clientesNotFiltered.filter(n => n.text.toUpperCase().indexOf(value.toUpperCase()) !== -1)
    })
  }

  editClientes = (event) => {
    const id = event.target.id
    const index = this.state.clientes.map(n => n.id.toString()).indexOf(id)
    const clientes = this.state.clientes
    clientes[index].editing = true
    this.setState({
      clientes: clientes
    })

  }

  saveEditCliente = async (event) => {
    const id = event.target.id
    const index = this.state.clientes.map(n => n.id.toString()).indexOf(id)
    const clientes = this.state.clientes
    clientes[index].editing = false
    this.setState({
      clientes: clientes
    })
    await saveEditClienteService(id, this.state.text, this.state.CPF, this.state.email)
    this.componentDidMount()
  }


  render() {
    return (
      <div>

        <div className='content'>

          <div className='filters'>

            <div className='right'>
              <ButtonToolbar>
                <Button variant="dark" onClick={this.newForn}>Novo Cliente</Button>
              </ButtonToolbar>

            </div>
          </div>

          <hr />

          <div className='list'>

            {this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
                </div>
            )}
            {!this.state.clientes.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            )}
            {this.state.new && (
              <div className='item'>
                <input disabled={this.state.loadingSave} onKeyPress={this.keyPress} autoFocus onChange={this.editText} className='left' type='text' placeholder='Digite sua anotação' />
                <div className='right'>
                  {!this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>)}
                  {this.state.loadingSave && (<span>Salvando...</span>)}
                </div>
              </div>
)}

              <div className='table'>
                <Table striped bordered hover>

                  <thead>
                    <tr>
                      <th>id</th>
                      <th>Nome</th>
                      <th>CPF</th>
                      <th>Email</th>
                      <th>Opções</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.clientes.map(cliente => (
                      <tr>
                        <td>
                          <div>
                            {cliente.id}
                          </div>
                        </td>
                        <td>
                          <div className='item'>
                            <input onChange={this.editText} className='left' type='text' defaultValue={cliente.text} placeholder='Digite sua anotação' />
                          </div>
                        </td>
                        <td>
                          <div>
                            {cliente.CPF}
                          </div>
                        </td>
                        <td>
                          <div>
                            {cliente.email}
                          </div>
                        </td>
                        <td>
                          <div className='right'>
                            {(!cliente.id || cliente.editing) && (<button onClick={this.saveEditCliente} id={cliente.id}>Salvar</button>)}
                            {cliente.id && !cliente.editing && (<button id={cliente.id} onClick={this.delCliente}>Excluir</button>)}
                            {cliente.id && !cliente.editing && (<button id={cliente.id} onClick={this.editCliente}>Editar</button>)}
                          </div>


                        </td>
                      </tr>
                    ))}

                  </tbody>
                </Table>
              </div>



          </div>
        </div >
        </div >
        )
      }
}