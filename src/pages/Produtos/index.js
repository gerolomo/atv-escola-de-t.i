import React, { Component } from 'react'
import { Button, ButtonToolbar, Table } from 'react-bootstrap'

import { listProdutos, saveProdutos, saveEditProdutoService, removeProduto } from '../../services/serviceProdutos.js'

import './style.css'

export default class Produtos extends Component {
  state = {
    produtos: [],
    produtosNotFiltered: [],
    loading: true,
    loadingSave: false,
    new: false,
    search: '',
    nome: '',
    codProduto: ''
  }

  async componentDidMount() {
    this.setState({
      produtosNotFiltered: await listProdutos(),
      loading: false
    })
    this.filter()
  }


  newProduto = () => {
    this.setState({
      new: true
    })
  }

  editnome = (event) => {
    this.setState({
      nome: event.target.value
    })
  }

  saveProdutos = async () => {
    this.setState({
      loadingSave: true
    })
    await saveProdutos(this.state.nome, this.state.codProduto)
    this.setState({
      produtos: await listProdutos(),
      loadingSave: false,
      new: false,
      nome: '',
      codProduto: ''
    })
  }

  keyPress = (event) => {
    if (event.key === 'Enter') {
      this.saveProdutos()
    }
  }

  setFilter = (event) => {
    this.setState({
      search: event.target.value
    })
    this.filter(event.target.value)
  }

  filter = (value) => {
    value = !value ? this.state.search : value
    this.setState({
      produtos: this.state.produtosNotFiltered.filter(n => n.nome.toUpperCase().indexOf(value.toUpperCase()) !== -1)
    })
  }

  editProduto = (event) => {
    const id = event.target.id
    const index = this.state.produtos.map(n => n.id.toString()).indexOf(id)
    const produtos = this.state.produtos
    //produtos[index].editing = true
    this.setState({
      produtos: produtos
    })

  }

  saveeditProduto = async (event) => {
    const id = event.target.id
    const index = this.state.produtos.map(n => n.id.toString()).indexOf(id)
    const produtos = this.state.produtos
    produtos[index].editing = false
    this.setState({
      produtos: produtos
    })
    await saveEditProdutoService(id, this.state.nome, this.state.codProduto)
    this.componentDidMount()
  }


  render() {
    return (
      <div>
        <div className='content'>
          <div className='filters'>
            <div className='right'>
              <ButtonToolbar>
                <Button variant="dark" onClick={this.newProduto}>Novo Produto</Button>
              </ButtonToolbar>
            </div>
          </div>

          <hr />
         
          <div className='list'>
            {this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
                </div>
            )}
            {!this.state.produtos.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            )}
            {this.state.new && (
              <div className='item'>
                <input disabled={this.state.loadingSave} onKeyPress={this.keyPress} autoFocus onChange={this.editProduto} className='left' type='CodProduto' placeholder='Código Produto' />
                <input disabled={this.state.loadingSave} onKeyPress={this.keyPress} autoFocus onChange={this.editnome} className='left' type='nome' placeholder='Nome Produto' />
                <div className='right'>
                  {!this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>)}
                  {this.state.loadingSave && (<span>Salvando...</span>)}
                </div>
              </div>
)}
              <div className='table'>
                <Table striped bordered hover>

                  <thead>
                    <tr>
                      <th>id</th>
                      <th>Nome</th>
                      <th>codProduto</th>
                    </tr>
                  </thead>
                  <tbody>

                    {this.state.produtos.map(produtos => (
                      <tr>
                        <td>
                          <div>
                            {produtos.id}
                          </div>
                        </td> 
                        <td>
                          <div>
                            {produtos.nome}
                          </div>
                        </td>
                        <td>
                          <div>
                            {produtos.codProduto}
                          </div>
                        </td>
                        <td>
                          <div className='right'>
                            {(!produtos.id || produtos.editing) && (<button onClick={this.saveeditProduto} id={produtos.id}>Salvar</button>)}
                            {produtos.id && !produtos.editing && (<button id={produtos.id} onClick={this.delProduto}>Excluir</button>)}
                            {produtos.id && !produtos.editing && (<button id={produtos.id} onClick={this.editProduto}>Editar</button>)}
                          </div>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
          </div>
        </div >
        </div >
        )
      }
}
