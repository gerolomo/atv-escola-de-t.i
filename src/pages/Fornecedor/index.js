import React, { Component } from 'react';
import { Button, ButtonToolbar, Spinner, Table, Modal } from 'react-bootstrap';


import { listForns, saveForns, saveEditFornService, removeForn } from '../../services/serviceFornecedor.js'


export default class Fornecedor extends Component {

  state = {
    forns: [],
    fornsNotFiltered: [],
    loading: true,
    loadingSave: false,
    new: false,
    search: '',
    text: '',
    CPF: '',
    email: ''
  }

  async componentDidMount() {
    this.setState({
      fornsNotFiltered: await listForns(),
      loading: false
    })
    this.filter()
  }


  newForn = () => {
    this.setState({
      new: true
    })
  }

  editText = (event) => {
    this.setState({
      text: event.target.value
    })
  }

  saveForn = async () => {
    this.setState({
      loadingSave: true
    })
    await saveForns(this.state.text, this.state.CPF, this.state.email)
    this.setState({
      forns: await listForns(),
      loadingSave: false,
      new: false,
      text: '',
      CPF: '',
      email: ''
    })
  }

  keyPress = (event) => {
    if (event.key === 'Enter') {
      this.saveForn()
    }
  }


  setFilter = (event) => {
    this.setState({
      search: event.target.value
    })
    this.filter(event.target.value)
  }

  filter = (value) => {
    value = !value ? this.state.search : value
    this.setState({
      forns: this.state.fornsNotFiltered.filter(n => n.text.toUpperCase().indexOf(value.toUpperCase()) !== -1)
    })
  }

  editForn = (event) => {
    const id = event.target.id
    const index = this.state.forns.map(n => n.id.toString()).indexOf(id)
    const forns = this.state.forns
    forns[index].editing = true
    this.setState({
      forns: forns
    })

  }

  saveEditForn = async (event) => {
    const id = event.target.id
    const index = this.state.forns.map(n => n.id.toString()).indexOf(id)
    const forns = this.state.forns
    forns[index].editing = false
    this.setState({
      forns: forns
    })
    await saveEditFornService(id, this.state.text, this.state.CPF, this.state.email)
    this.componentDidMount()
  }


  render() {
    return (
      <div>
        <div className='content'>
          <div className='filters'>
            <div className='right'>
              <ButtonToolbar>
                <Button variant="dark" onClick={this.newForn}>Novo Fornecedor</Button>
              </ButtonToolbar>
            </div>
          </div>

          <hr />
         
          <div className='list'>
            {this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
                </div>
            )}
            {!this.state.forns.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            )}
            {this.state.new && (
              <div className='item'>
                <input disabled={this.state.loadingSave} onKeyPress={this.keyPress} autoFocus onChange={this.editText} className='left' type='text' placeholder='Digite sua anotação' />
                <div className='right'>
                  {!this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>)}
                  {this.state.loadingSave && (<span>Salvando...</span>)}
                </div>
              </div>
)}
              <div className='table'>
                <Table striped bordered hover>

                  <thead>
                    <tr>
                      <th>id</th>
                      <th>Nome</th>
                      <th>CPF</th>
                      <th>Email</th>
                      <th>Opções</th>
                    </tr>
                  </thead>
                  <tbody>

                    {this.state.forns.map(forn => (
                      <tr>
                        <td>
                          <div>
                            {forn.id}
                          </div>
                        </td>
                        <td>
                          <div className='item'>
                            <input onChange={this.editText} className='left' type='text' defaultValue={forn.text} placeholder='Digite sua anotação' />
                          </div>
                        </td>
                        <td>
                          <div>
                            {forn.CPF}
                          </div>
                        </td>
                        <td>
                          <div>
                            {forn.email}
                          </div>
                        </td>
                        <td>
                          <div className='right'>
                            {(!forn.id || forn.editing) && (<button onClick={this.saveEditForn} id={forn.id}>Salvar</button>)}
                            {forn.id && !forn.editing && (<button id={forn.id} onClick={this.delForn}>Excluir</button>)}
                            {forn.id && !forn.editing && (<button id={forn.id} onClick={this.editForn}>Editar</button>)}
                          </div>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
          </div>
        </div >
        </div >
        )
      }
}