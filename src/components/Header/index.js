import React, { Component } from 'react'
import {Navbar,Nav,NavDropdown} from 'react-bootstrap'

import { Link } from 'react-router-dom'

import './style.css'

export default class Header extends Component {
  render () {
    return (
      <div className='header'>
        <Navbar bg="light" expand="lg">
  <Navbar.Brand href="/">Pedidos Web</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href="/produtos">Produtos</Nav.Link>
      <Nav.Link href="/clientes">Clientes</Nav.Link>
      <Nav.Link href="/fornecedor">Fornecedor</Nav.Link>
      
    </Nav>
   </Navbar.Collapse>
</Navbar>
      </div>      
    )

    
  }
}
