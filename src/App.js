import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css'
import { FaBeer } from 'react-icons/fa'
import Navbar from 'react-bootstrap/Navbar'

import PedidosPage from './pages/Pedidos'
import ProdutosPage from './pages/Produtos'
import ClientesPage from './pages/Clientes'
import FornecedorPage from './pages/Fornecedor'
import Header from './components/Header'

class App extends Component {
  render () {
    return (
      <BrowserRouter>
        <div>
          <Header />
          <Switch>
            <Route path='/' exact component={PedidosPage} />
            <Route path='/Produtos' component={ProdutosPage} />
            <Route path='/Clientes' component={ClientesPage} />
            <Route path='/Fornecedor' component={FornecedorPage} />
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

export default App
