const result = [
  { text: 'Joao', id: new Date().getTime(), CPF: '090909090909', email: 'teste@teste.com' },
  { text: 'pedro', id: new Date().getTime(), CPF: '090909090909', email: 'teste2@teste2.com' }
]

const listForns = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(result)
    }, 2000)
  })
}

const saveForns = (text, CPF, email) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      result.unshift({
        text: text,
        id: new Date().getTime(),
        email: email,
        CPF: CPF
      })
      resolve()
    }, 2000)
  })
}
const removeForn = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const index = result.map(e => e.id.toString()).indexOf(id)
      delete result[index]
      resolve()
    }, 200)
  })
}

const saveEditFornService = (id, text) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const index = result.map(e => e.id.toString()).indexOf(id)
      result[index].text = text
      console.log(result)
      resolve()
    }, 200)
  })
}

module.exports = {
  listForns,
  saveForns,
  removeForn,
  saveEditFornService
}
