const result = [
  { nome: 'Feijão', id: new Date().getTime(), codProduto: '090909090909' },
  { nome: 'Arroz', id: new Date().getTime(), codProduto: '090909090909' }
]

const listProdutos = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(result)
    }, 2000)
  })
}

const saveProdutos = (nome, codProduto) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      result.unshift({
        nome: nome,
        id: new Date().getTime(),
        codProduto: codProduto
      })
      resolve()
    }, 2000)
  })
}
const removeProdutos = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const index = result.map(e => e.id.toString()).indexOf(id)
      delete result[index]
      resolve()
    }, 200)
  })
}

const saveEditPordutoService = (id, nome) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const index = result.map(e => e.id.toString()).indexOf(id)
      result[index].nome = nome
      console.log(result)
      resolve()
    }, 200)
  })
}

module.exports = {
  listProdutos,
  saveProdutos,
  removeProdutos,
  saveEditPordutoService
}
