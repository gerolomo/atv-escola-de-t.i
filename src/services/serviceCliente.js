const result = [
  { text: 'Rodrigo', id: new Date().getTime(), CPF: '090909090909', email: 'rodrigo@teste.com' },
  { text: 'Alison', id: new Date().getTime(), CPF: '090909090909', email: 'alison@teste.com' }
]

const listClientes = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(result)
    }, 2000)
  })
}

const saveClientes = (text, CPF, email) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      result.unshift({
        text: text,
        id: new Date().getTime(),
        email: email,
        CPF: CPF
      })
      resolve()
    }, 2000)
  })
}
const removeCliente = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const index = result.map(e => e.id.toString()).indexOf(id)
      delete result[index]
      resolve()
    }, 200)
  })
}

const saveEditClienteService = (id, text) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const index = result.map(e => e.id.toString()).indexOf(id)
      result[index].text = text
      console.log(result)
      resolve()
    }, 200)
  })
}

module.exports = {
  listClientes,
  saveClientes,
  removeCliente,
  saveEditClienteService
}
